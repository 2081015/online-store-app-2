CREATE TABLE IF NOT EXISTS "users" (
  "id" varchar not null PRIMARY KEY,
  "email" varchar,
  "password" varchar,
  "cash" float,
  "created_at" timestamp,
  "deleted_at" timestamp
);