CREATE TABLE IF NOT EXISTS "orders" (
  "id" varchar not null PRIMARY KEY,
  "users_id" varchar,
  "products_id" varchar,
  "quantity" integer,
  "price" float,
  "order_status_id" integer,
  "created_at" timestamp,
  "deleted_at" timestamp
);