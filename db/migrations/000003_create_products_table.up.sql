CREATE TABLE IF NOT EXISTS "products" (
  "id" varchar not null PRIMARY KEY,
  "product_categories_id" varchar,
  "name" varchar,
  "price" float,
  "stock" int,
  "created_at" timestamp,
  "deleted_at" timestamp
);


INSERT INTO "products" ("id","product_categories_id","name","price","stock","created_at") 
VALUES 
('PRODUCTKNSLA95L', 'PRODUCTCATJNSLA96L','ABON SAPI 200GR',50000.00,10,NOW()),
('PRODUCTKNSLA55L', 'PRODUCTCATJNSLA96L','SUSU SEGAR X 5L',1000000.00,10,NOW()),
('PRODUCTKNSLA56L', 'PRODUCTCATJNSLA95L','SEPATU CATS ALL SIZE',5000000.00,10,NOW()),
('PRODUCTKNSLA57L', 'PRODUCTCATJNSLA95L','RAK BUKU XL ALL COLOR',8000000.00,10,NOW()),
('PRODUCTKNSLA58L', 'PRODUCTCATJNSLA95L','GALON KOSONGAN 5L',30000.00,10,NOW());


