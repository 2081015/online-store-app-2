CREATE TABLE IF NOT EXISTS "order_status" (
  "id" int not null PRIMARY KEY,
  "status" varchar
);

INSERT INTO "order_status" ("id", "status")
VALUES (1, 'In cart'), (2, 'Paid');
