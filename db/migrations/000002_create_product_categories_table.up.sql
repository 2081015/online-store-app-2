CREATE TABLE IF NOT EXISTS "product_categories" (
  "id" varchar not null PRIMARY KEY,
  "type" varchar,
  "created_at" timestamp,
  "deleted_at" timestamp
);


INSERT INTO "product_categories" ("id","type","created_at") VALUES ('PRODUCTCATJNSLA95L', 'Goods',NOW()), ('PRODUCTCATJNSLA96L', 'Foods and Beverage', NOW());

