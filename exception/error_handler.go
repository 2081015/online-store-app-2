package exception

import (
	"errors"
	"online-store-app-synapsis-2/model"

	"github.com/gofiber/fiber/v2"
)

func ErrorHandler(ctx *fiber.Ctx, err error) error {
	code := fiber.StatusInternalServerError

	var e *fiber.Error
	if errors.As(err, &e) {
		code = e.Code
	}

	response := model.WebResponse{
		Code:    code,
		Status:  false,
		Message: err.Error(),
	}

	return ctx.Status(code).JSON(response)

}
