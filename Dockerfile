# Builder Stage
FROM golang:1.21.0 as builder
ARG CGO_ENABLED=0
WORKDIR /app

COPY .env go.mod go.sum ./
RUN go mod download
COPY . .

RUN go build -o /app/server .


FROM scratch
COPY --from=builder /app/server /server

EXPOSE 10104
ENTRYPOINT ["/server"]


