package config

import "online-store-app-synapsis-2/utils"

var (
	//db

	host     = utils.GetEnv("DB_HOST")
	port     = utils.GetEnv("DB_PORT")
	user     = utils.GetEnv("DB_USERNAME")
	password = utils.GetEnv("DB_PASSWORD")
	dbName   = utils.GetEnv("DB_NAME")
	minConns = utils.GetEnv("DB_POOL_MIN")
	maxConns = utils.GetEnv("DB_POOL_MAX")

	//host
	ServerHost            = utils.GetEnv("SERVER_URI")
	ServerPort            = utils.GetEnv("SERVER_PORT")
	EndpointPrefixAuth    = utils.GetEnv("ENDPOINT_PREFIX_AUTH")
	EndpointPrefixUser    = utils.GetEnv("ENDPOINT_PREFIX_USER")
	EndpointPrefixProduct = utils.GetEnv("ENDPOINT_PREFIX_PRODUCT")
	EndpointPrefixOrder   = utils.GetEnv("ENDPOINT_PREFIX_ORDER")
	EndpointPrefixPublic  = utils.GetEnv("ENDPOINT_PREFIX_PUBLIC")
	DefaultLimit          = utils.GetEnv("DEFAULT_LIMIT")

	//jwt
	SecretKey           = utils.GetEnv("SECRET_KEY")
	SecretKeyRefresh    = utils.GetEnv("SECRET_KEY_REFRESH")
	SessionLogin        = utils.GetEnv("SESSION_LOGIN")
	SessionRefreshToken = utils.GetEnv("SESSION_REFRESH_TOKEN")

	//variable
	VariableRoleAdmin    = utils.GetEnv("ROLE_ADMIN")
	VariableRoleUser     = utils.GetEnv("ROLE_USER")
	VariableRoleEmployee = utils.GetEnv("ROLE_EMPLOYEE")
	CategoryService      = utils.GetEnv("CATEGORY_SERVICE")
	PermissionManageUser = utils.GetEnv("PERMISSION_MANAGE_USER")

	//minio
	Minio_storage_url = utils.GetEnv("MINIO")
	Minio_storage_    = utils.GetEnv("MINIO")
	MinioPrefix       = utils.GetEnv("MINIO_PREFIX")
	MinioAccessKey    = utils.GetEnv("MINIO_ACCESSKEY")
	MinioSecretKey    = utils.GetEnv("MINIO_SECRETKEY")
	MinioEndpoint     = utils.GetEnv("MINIO_ENDPOINT")
	MinioBucket       = utils.GetEnv("MINIO_BUCKET")
	MinioPath         = utils.GetEnv("MINIO_PATH")
	MinioSSL          = utils.GetEnv("MINIO_SSL")

	//gcs
	GCSBucket   = utils.GetEnv("GSC_BUCKET")
	GCSOSPREFIX = utils.GetEnv("GCS_OS_PREFIX")

	// transaction
	MidtransServerKey     = utils.GetEnv("MIDTRANS_SERVER_KEY")
	MidtransApiPrefix     = utils.GetEnv("MIDTRANS_API_PREFIX")
	ChargeEndpoint        = utils.GetEnv("CHARGE_EP")
	CancelPendingEndpoint = utils.GetEnv("CANCEL_EP")
	RefundEndpoint        = utils.GetEnv("REFUND_EP")
	BriVaRate             = utils.GetEnv("BRI_VA_RATE")

	OrderIdSep = utils.GetEnv("ORDER_ID_SEP")
	OrderDP    = utils.GetEnv("ORDER_DP")

	// //kafka
	// KafkaHost              = utils.GetEnv("KAFKA_HOST")
	// KafkaPort              = utils.GetEnv("KAFKA_PORT")
	// KafkaTopic             = utils.GetEnv("KAFKA_TOPIC")
	// KafkaLogTopic          = utils.GetEnv("KAFKA_LOG_TOPIC")
	// KafkaTopicDepartement  = utils.GetEnv("KAFKA_TOPIC_DEPARTEMENT")
	// KafkaTopicSiteLocation = utils.GetEnv("KAFKA_TOPIC_SITE_LOCATION")
	// KafkaTopicProject      = utils.GetEnv("KAFKA_TOPIC_PROJECT")
	// KafkaConsumerGroup     = utils.GetEnv("KAFKA_CONSUMER_GROUP")
	// KafkaAddressFamily     = utils.GetEnv("KAFKA_ADDRESS_FAMILY")
	// KafkaSessionTimeout, _ = strconv.Atoi(utils.GetEnv("KAFKA_SESSION_TIMEOUT"))
	// KafkaAutoOffsetReset   = utils.GetEnv("KAFKA_AUTO_OFFSET_RESET")
)
