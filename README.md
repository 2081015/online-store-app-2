# Synapsis Software Engineer Challenge

This challenge is to make a service of **Online Store Application** using Go, exactly i use Go to land off this project.
_Synapsis challenge of fulltime recruitment._
(manurungnico02@gmail.com)

This API is a pretty basic implementation of an online store.

## Directory Structure

Basicly, i was using the codebase by Synapsis workspace with a simple modification.

```
config/
|-
db/migrations/
|-
exceptions/
|-
helper/
|-
middleware/
|-
model/
|-
modules/
|-
services/                                 *this repo has got rid from passing the injection things to interfaces contract - im still exploring about benchmarking the time exection by heap and stack process allocation .
|-
services/
main.go
```

## Environment Variables

To run this project, you will need to copy .env.examples to your .env file.<br><br>

# Endpoints.

## Register

```http
  POST /auth/register
```

```http
Payload :
{
	"email": "user1@add.com",
	"password": "password",
	"cash":200000
}
```

```http
Response :
{
    "code": 200,
    "status": true,
    "message": "register success",
    "data": {
        "id": "USER37940412",
        "email": "user1@add.com",
        "cash": 200000,
        "createdAt": "2024-05-05T15:51:41.1366641Z",
        "deletedAt": null
    }
}
```

## Login

```http
  POST /auth/login
```

```http
Payload :
{
	"email": "user1@add.com",
	"password": "password"
}
```

```http
Response :
{
    "code": 200,
    "status": true,
    "message": "register success",
    "data": {
        "id": "USER37940412",
        "email": "user1@add.com",
        "cash": 200000,
        "createdAt": "2024-05-05T15:51:41.1366641Z",
        "deletedAt": null
    }
}
```

## Show All Products By Category

```http
    GET /product/in/category[?catId]
```

```http
Responses :
{
    "code": 200,
    "status": true,
    "message": "Get Product List By Product Category success",
    "data": [
        {
            "id": "PRODUCTKNSLA55L",
            "productCategoryId": "PRODUCTCATJNSLA96L",
            "name": "SUSU SEGAR X 5L",
            "price": 1000000,
            "stock": 10,
            "createdAt": "2024-05-05T15:50:42.698725Z",
            "deletedAt": null
        },
        {
            "id": "PRODUCTKNSLA95L",
            "productCategoryId": "PRODUCTCATJNSLA96L",
            "name": "ABON SAPI 200GR",
            "price": 50000,
            "stock": 6,
            "createdAt": "2024-05-05T15:50:42.698725Z",
            "deletedAt": null
        }
    ]
}
```

## Add item to cart

```http
  POST /order/cart
```

```http
Payload :
{
    "product_id": "PRODUCTKNSLA95L",
    "quantity" : 3
}
```

```http
Response :
{
    "code": 201,
    "status": true,
    "message": "Add to cart success",
    "data": null
}
```

## Find items of cart

```http
  GET /order/cart
```

```http
Response :
{
    "code": 200,
    "status": true,
    "message": "Find Cart By User Id success",
    "data": [
        {
            "id": "ORDER36DF681A",
            "userId": "USER37940412",
            "productId": "PRODUCTKNSLA95L",
            "quantity": 1,
            "price": 50000,
            "orderStatusId": 1,
            "createdAt": "2024-05-05T16:19:21.213229Z",
            "deletedAt": null
        }
    ]
}
```

## Checkout items of cart

```http
  PUT /order/cart
```

```http
Payload :
{
    "items": [
          {
            "id": "ORDER20014258",
            "userId": "USER37940412",
            "productId": "PRODUCTKNSLA95L",
            "quantity": 3,
            "price": 150000,
            "orderStatusId": 1,
            "createdAt": "2024-05-05T15:52:19.183135Z",
            "deletedAt": null
        }
    ]
}
```

```http
Response :
{
    "code": 201,
    "status": true,
    "message": "Checkout cart success",
    "data": null
}
```

## Delete items from cart

```http
  DELETE /order/cart
```

```http
Payload :
{

    "items": [
            {
            "id": "ORDER6BDE3257",
            "userId": "USER37940412",
            "productId": "PRODUCTKNSLA95L",
            "quantity": 3,
            "price": 150000,
            "orderStatusId": 1,
            "createdAt": "2024-05-05T15:52:08.309996Z",
            "deletedAt": null
        }
    ]
}
```

```http
Response :
{
    "code": 200,
    "status": true,
    "message": "Delete Carts success",
    "data": null
}
```
