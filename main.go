package main

import "online-store-app-synapsis-2/modules"

func main() {
	modules.RunModules()
}
