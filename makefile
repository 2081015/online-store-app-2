run:
	go run main.go

migrate-file:
	migrate create -ext sql -dir db/migrations -seq create_order_status_table

mr-up:
	migrate -database postgresql://postgres:sec@localhost:5430/online-app-store?sslmode=disable -path db/migrations up

mr-down:
	migrate -database postgresql://postgres:sec@localhost:5430/online-app-store?sslmode=disable -path db/migrations down	

build-service:
	docker run --name osa-ifc-prevent -p 10104:10104 -d --env-file .env --network osa-network osa 

build-db:
	docker run --name postgres -e POSTGRES_PASSWORD=sec -d --network osa-network -p 5430:5432 postgres

