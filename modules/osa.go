package modules

import (
	"online-store-app-synapsis-2/config"
	OrderController "online-store-app-synapsis-2/services/orders/controller"
	OrderRepository "online-store-app-synapsis-2/services/orders/repository"
	OrderUsecase "online-store-app-synapsis-2/services/orders/usecase"
	ProductController "online-store-app-synapsis-2/services/products/controller"
	ProductRepository "online-store-app-synapsis-2/services/products/repository"
	ProductUsecase "online-store-app-synapsis-2/services/products/usecase"
	UserController "online-store-app-synapsis-2/services/users/controller"
	UserRepository "online-store-app-synapsis-2/services/users/repository"
	UserUsecase "online-store-app-synapsis-2/services/users/usecase"
	"time"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
)

/* osa: online-store-app */
func OSA(app *fiber.App) {

	time.Local = time.UTC
	db := config.NewPostgresPool()
	validate := validator.New()

	// domain setup
	/* -- user -- */
	ur := UserRepository.UserRepository{Database: db}
	us := UserUsecase.UserUsecase{Validate: validate, UserRepository: &ur}
	uc := UserController.UserController{UserUsecase: &us}

	/* -- product -- */
	pr := ProductRepository.ProductRepository{Database: db}
	ps := ProductUsecase.ProductUsecase{ProductsRepository: &pr}
	pc := ProductController.ProductController{ProductUsecase: &ps}

	/* -- order -- */
	or := OrderRepository.OrderRepository{Database: db}
	os := OrderUsecase.OrderUsecase{Validate: validate, OrderRepository: &or, UserRepository: &ur, ProductRepository: &pr}
	oc := OrderController.OrderController{OrderUsecase: &os}

	// run endpoint
	uc.NewAuthController(app)
	pc.NewProductController(app)
	oc.NewOrderController(app)

}
