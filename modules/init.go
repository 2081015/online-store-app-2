package modules

import (
	"fmt"
	"online-store-app-synapsis-2/config"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
)

func RunModules() {
	// context setup
	app := fiber.New(fiber.Config{BodyLimit: 10 * 1024 * 1024})
	app.Use(cors.New(cors.Config{
		AllowOrigins:     "*",
		AllowMethods:     "*",
		AllowHeaders:     "*",
		AllowCredentials: false,
	}))

	// invoke modules
	OSA(app)

	// run server
	host := fmt.Sprintf("%s:%s", config.ServerHost, config.ServerPort)
	err := app.Listen(host)
	if err != nil {
		fmt.Println(err)
		return
	}

}
