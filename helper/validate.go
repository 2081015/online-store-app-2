package helper

import (
	"online-store-app-synapsis-2/exception"
	"regexp"
	"strings"
)

func IsEmailValid(e string) bool {
	emailRegex := regexp.MustCompile(`^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$`)
	return emailRegex.MatchString(e)
}

func IsDateValid(e string) bool {
	dateRegex := regexp.MustCompile(`^(15\d{2}|1[6-9]\d{2}|2\d{3})-(0[1-9]|1[0-2])-(0[1-9]|[12][0-9]|3[01])$`)
	return dateRegex.MatchString(e)
}

func ValidateStruct(err error, opt ...string) error {
	if strings.Contains(err.Error(), "no rows in result set") {
		return exception.ErrorBadRequest("Data " + opt[0] + " Not Found")
	}
	if strings.Contains(err.Error(), "RoleId") && strings.Contains(err.Error(), "required") {
		return exception.ErrorBadRequest("Role required.")
	}
	if strings.Contains(err.Error(), "GenderId") && strings.Contains(err.Error(), "required") {
		return exception.ErrorBadRequest("Gender required.")
	}
	if strings.Contains(err.Error(), "Fullname") && strings.Contains(err.Error(), "required") {
		return exception.ErrorBadRequest("Fullname required.")
	}
	if strings.Contains(err.Error(), "BirthDate") && strings.Contains(err.Error(), "required") {
		return exception.ErrorBadRequest("BirthDate required.")
	}
	if strings.Contains(err.Error(), "Email") && strings.Contains(err.Error(), "required") {
		return exception.ErrorBadRequest("Email required.")
	}
	if strings.Contains(err.Error(), "Password") && strings.Contains(err.Error(), "required") {
		return exception.ErrorBadRequest("Password required.")
	}
	if strings.Contains(err.Error(), "PhoneNumber") && strings.Contains(err.Error(), "required") {
		return exception.ErrorBadRequest("PhoneNumber required.")
	}
	if strings.Contains(err.Error(), "ImageProfile") && strings.Contains(err.Error(), "required") {
		return exception.ErrorBadRequest("ImageProfile required.")
	}
	return exception.ErrorBadRequest(err.Error())
}
