package helper

import (
	"online-store-app-synapsis-2/config"
	"strconv"
	"time"

	"github.com/golang-jwt/jwt"
	_ "github.com/joho/godotenv/autoload"
)

// type PermissionRole struct {
// 	PermissionId   interface{} `json:"id"`
// 	PermissionName interface{} `json:"name"`
// }

type Claims struct {
	UserId string `json:"user_id"`
	Role   string `json:"role"`
	jwt.StandardClaims
}

func GenerateJwt(issuer, id, level string) (string, error) {
	session, _ := strconv.Atoi(config.SessionLogin)
	claims := &Claims{
		UserId: id,
		Role:   level,
		StandardClaims: jwt.StandardClaims{
			Issuer:    issuer,
			ExpiresAt: time.Now().Add(time.Hour * time.Duration(session)).Unix(),
		},
	}
	tokens := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	return tokens.SignedString([]byte(config.SecretKey))
}

func ParseJwt(cookie string) (string, string, string, error) {
	var claims Claims
	token, err := jwt.ParseWithClaims(cookie, &claims, func(token *jwt.Token) (interface{}, error) {
		return []byte(config.SecretKey), nil
	})
	if err != nil || !token.Valid {
		return "", "", "", err
	}

	return claims.Issuer, claims.UserId, claims.Role, nil
}

// refresh
func GenerateRefreshJwt(issuer string) (string, error) {
	session, _ := strconv.Atoi(config.SessionRefreshToken)
	claims := &Claims{
		StandardClaims: jwt.StandardClaims{
			Issuer:    issuer,
			ExpiresAt: time.Now().Add(time.Hour * time.Duration(session)).Unix(),
		},
	}
	tokens := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	return tokens.SignedString([]byte(config.SecretKeyRefresh))
}

func ParseRefreshJwt(cookie string) (string, error) {
	var claims jwt.StandardClaims
	token, err := jwt.ParseWithClaims(cookie, &claims, func(token *jwt.Token) (interface{}, error) {
		return []byte(config.SecretKeyRefresh), nil
	})
	if err != nil || !token.Valid {
		return "", err
	}

	return claims.Issuer, err
}
