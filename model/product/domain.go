package product

import "time"

// Product represents the Products table
type Product struct {
	ID                string     `json:"id"`
	ProductCategoryID string     `json:"productCategoryId"`
	Name              string     `json:"name"`
	Price             float64    `json:"price"`
	Stock             int        `json:"stock"`
	CreatedAt         *time.Time `json:"createdAt"`
	DeletedAt         *time.Time `json:"deletedAt"`
}
