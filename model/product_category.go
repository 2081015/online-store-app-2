package model

import "time"

// ProductCategory represents the Product Categories table
type ProductCategory struct {
	ID        int       `json:"id"`
	Type      string    `json:"type"`
	CreatedAt *time.Time `json:"createdAt"`
	DeletedAt *time.Time `json:"deletedAt"`
}
