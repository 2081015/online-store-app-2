package order

// Order represents the Orders table
type OrderCreateDTO struct {
	UserID    string
	ProductID string `json:"product_id" validate:"required"`
	Quantity  int    `json:"quantity" validate:"required"`
	// rate calculated by be
}

type OrderRemoveDTO struct {
	Items []Order `json:"items"`
}

type OrderCheckoutDTO struct {
	Items []Order `json:"items"`
}
