package order

import "time"

// Order represents the Orders table
type Order struct {
	ID            string     `json:"id"`
	UserID        string     `json:"userId"`
	ProductID     string     `json:"productId"`
	Quantity      int        `json:"quantity"`
	Price         float64    `json:"price"`
	OrderStatusID int        `json:"orderStatusId"`
	CreatedAt     *time.Time `json:"createdAt"`
	DeletedAt     *time.Time `json:"deletedAt"`
}
