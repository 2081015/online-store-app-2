package model

// OrderStatus represents the Order Status table
type OrderStatus struct {
	ID     int    `json:"id"`
	Status string `json:"status"`
}
