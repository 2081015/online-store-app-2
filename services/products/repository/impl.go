package repository

import (
	"context"
	"fmt"
	"online-store-app-synapsis-2/model/product"
	"strings"

	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgxpool"
)

type ProductRepository struct {
	Database *pgxpool.Pool
}

func (ur *ProductRepository) BeginTx(ctx context.Context) (db pgx.Tx, err error) {
	db, err = ur.Database.Begin(ctx)
	return
}

func (pr *ProductRepository) GetProductListByProductCategory(ctx context.Context, catId string) (res []product.Product, err error) {
	tx, err := pr.BeginTx(ctx)
	if err != nil {
		return
	}

	var cond string
	if len(catId) > 0 {
		cond = fmt.Sprintf("and pc.id = '%s'", catId)
	}

	query := fmt.Sprintf(`
	select p.* from products p
    inner join product_categories pc on p.product_categories_id = pc.id %s`, cond)
	fmt.Println(query)
	rows, err := tx.Query(ctx, query)
	if err != nil {
		return
	}

	res, err = pgx.CollectRows(rows, pgx.RowToStructByPos[product.Product])
	if err != nil {
		fmt.Printf("CollectRows error: %v", err)
		return
	}

	tx.Commit(ctx)

	return

}

func (pr *ProductRepository) GetProductById(ctx context.Context, id string) (res product.Product, err error) {
	tx, err := pr.BeginTx(ctx)
	if err != nil {
		return
	}

	query := fmt.Sprintf(`
	select * from products p where id = '%s'`, id)

	rows, err := tx.Query(ctx, query)
	if err != nil {
		return
	}

	res, err = pgx.CollectOneRow(rows, pgx.RowToStructByPos[product.Product])
	if err != nil {
		fmt.Printf("CollectRows error: %v", err)
		return
	}

	tx.Commit(ctx)

	return

}

func (ur *ProductRepository) BulkUpdateProductStock(ctx context.Context, tx pgx.Tx, sql []string) (err error) {

	query := fmt.Sprintf(`
	UPDATE products AS p 
	SET stock = v.stock
	FROM (
		VALUES
		 %s
	) AS v(id,stock) 
	WHERE p.id = v.id
	`, strings.Join(sql, ","))

	fmt.Println("sql: ", query)

	_, err = tx.Exec(ctx, query)
	if err != nil {
		return
	}

	return

}
