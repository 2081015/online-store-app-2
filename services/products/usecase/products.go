package usecase

import (
	"online-store-app-synapsis-2/model/product"
	"online-store-app-synapsis-2/services/products/repository"

	"github.com/gofiber/fiber/v2"
)

type ProductUsecase struct {
	ProductsRepository *repository.ProductRepository
}

func (pu *ProductUsecase) GetProductListByProductCategory(ctx *fiber.Ctx) (res []product.Product, err error) {

	catId := ctx.Query("catId", "")
	res, err = pu.ProductsRepository.GetProductListByProductCategory(ctx.Context(), catId)
	return

}
