package controller

import (
	"online-store-app-synapsis-2/config"
	"online-store-app-synapsis-2/exception"
	"online-store-app-synapsis-2/middleware"
	"online-store-app-synapsis-2/model"
	"online-store-app-synapsis-2/services/products/usecase"

	"github.com/gofiber/fiber/v2"
)

type ProductController struct {
	ProductUsecase *usecase.ProductUsecase
}

func (pc *ProductController) NewProductController(app *fiber.App) {
	product := app.Group(config.EndpointPrefixProduct)

	product.Get("/ping", func(ctx *fiber.Ctx) error {
		return ctx.Status(fiber.StatusOK).JSON(model.WebResponse{
			Code:    fiber.StatusOK,
			Status:  true,
			Message: "ok",
		})
	})

	product.Use(middleware.IsAuthenticated)
	product.Get("/in/category", pc.GetProductListByProductCategory)

}

func (pc *ProductController) GetProductListByProductCategory(ctx *fiber.Ctx) (err error) {
	res, err := pc.ProductUsecase.GetProductListByProductCategory(ctx)
	if err != nil {
		return exception.ErrorHandler(ctx, err)
	}

	return ctx.Status(fiber.StatusOK).JSON(model.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "Get Product List By Product Category success",
		Data:    res,
	})
}
