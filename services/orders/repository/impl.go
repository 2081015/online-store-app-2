package repository

import (
	"context"
	"fmt"
	"online-store-app-synapsis-2/model/order"
	"strings"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgxpool"
)

type OrderRepository struct {
	Database *pgxpool.Pool
}

func (ur *OrderRepository) BeginTx(ctx context.Context) (db pgx.Tx, err error) {
	db, err = ur.Database.Begin(ctx)
	return
}

func (ur *OrderRepository) CreateBatchOrder(ctx context.Context, request order.OrderCreateDTO) (err error) {

	tx, err := ur.BeginTx(ctx)
	if err != nil {
		return
	}

	id := "ORDER" + strings.ToUpper(uuid.NewString()[:8])

	price := fmt.Sprintf("(select price * %d from products where id = '%s')", request.Quantity, request.ProductID)

	query := fmt.Sprintf(`
		INSERT INTO orders (id, users_id, products_id, quantity, price, order_status_id, created_at)
		VALUES ('%s', '%s', '%s', %d, %s, %d, now());
	`, id, request.UserID, request.ProductID, request.Quantity, price, 1)

	fmt.Println("QUERY: ", query)

	_, err = tx.Exec(ctx, query)
	if err != nil {
		return
	}

	tx.Commit(ctx)

	return

}

func (ur *OrderRepository) FindCartByUserId(ctx context.Context, userId string) (res []order.Order, err error) {

	tx, err := ur.BeginTx(ctx)
	if err != nil {
		return
	}

	query := fmt.Sprintf(`
		SELECT * FROM orders WHERE users_id = '%s' AND order_status_id = 1
	`, userId)

	rows, err := tx.Query(ctx, query)
	if err != nil {
		return
	}

	res, err = pgx.CollectRows(rows, pgx.RowToStructByPos[order.Order])
	if err != nil {
		fmt.Printf("CollectRows error: %v", err)
		return
	}

	tx.Commit(ctx)

	return

}

func (ur *OrderRepository) UpdateCartStatus(ctx context.Context, tx pgx.Tx, ids []string, status int) (err error) {

	joined := strings.Join(ids, ",")
	query := fmt.Sprintf(`
	UPDATE orders SET order_status_id = %d WHERE id IN(%s)
	`, status, joined)
	fmt.Println(query)

	_, err = tx.Exec(ctx, query)
	if err != nil {
		return
	}

	return

}

func (ur *OrderRepository) DeleteCarts(ctx context.Context, ids []string) (err error) {

	tx, err := ur.BeginTx(ctx)
	if err != nil {
		return
	}

	joined := strings.Join(ids, ",")
	query := fmt.Sprintf(`
	delete from orders where id in(%s) and order_status_id = 1
	`, joined)

	fmt.Println(query)

	_, err = tx.Exec(ctx, query)
	if err != nil {
		return
	}

	tx.Commit(ctx)

	return

}
