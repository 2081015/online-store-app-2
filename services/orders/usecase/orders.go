package usecase

import (
	"fmt"
	"online-store-app-synapsis-2/helper"
	"online-store-app-synapsis-2/model/order"
	"online-store-app-synapsis-2/model/product"
	orders "online-store-app-synapsis-2/services/orders/repository"
	products "online-store-app-synapsis-2/services/products/repository"
	users "online-store-app-synapsis-2/services/users/repository"
	"strings"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
)

type OrderUsecase struct {
	Validate          *validator.Validate
	OrderRepository   *orders.OrderRepository
	UserRepository    *users.UserRepository
	ProductRepository *products.ProductRepository
}

/*
[func StoreCartObj] single page, single cart
*/
func (ou *OrderUsecase) StoreCartObj(ctx *fiber.Ctx, request order.OrderCreateDTO) (err error) {

	// validate creds
	cookie := ctx.Cookies("token")
	_, userId, _, err := helper.ParseJwt(cookie)
	if err != nil {
		return
	}

	if len(userId) <= 0 {
		err = fmt.Errorf("%s", "Empty Creds")
		return
	}
	request.UserID = userId

	err = ou.Validate.Struct(request)
	if err != nil {
		err = helper.ValidateStruct(err)
		return
	}

	// validate stock
	product, err := ou.ProductRepository.GetProductById(ctx.Context(), request.ProductID)
	if err != nil {
		err = helper.ValidateStruct(err, "product")
		return
	}

	if (product.Stock - request.Quantity) < 0 {
		err = fmt.Errorf("%s", "Insufficient stock")
		return
	}

	// validate cash
	user, err := ou.UserRepository.FindUserByParam(ctx.Context(), "id", strings.ToLower(request.UserID))
	if err != nil {
		err = helper.ValidateStruct(err, "user")
		return
	}

	if user.Cash-(product.Price*float64(request.Quantity)) < 0 {
		err = fmt.Errorf("%s", "Not enough balance")
		return
	}

	err = ou.OrderRepository.CreateBatchOrder(ctx.Context(), request)
	return err

}

func (ou *OrderUsecase) CheckOutCartObj(ctx *fiber.Ctx, request order.OrderCheckoutDTO) (err error) {

	var (
		prodStock = make(map[string]product.Product, 0) // to handle multiple same products with diff order id
		prodsql   []string                              // to update multiple prod
		orderIds  []string                              // to update multiple order
	)

	// validate creds
	cookie := ctx.Cookies("token")
	_, userId, _, err := helper.ParseJwt(cookie)
	if err != nil {
		return
	}

	err = ou.Validate.Struct(request)
	if err != nil {
		err = helper.ValidateStruct(err)
		return
	}

	user, err := ou.UserRepository.FindUserByParam(ctx.Context(), "id", strings.ToLower(userId))
	if err != nil {
		err = helper.ValidateStruct(err, "user")
		return
	}

	// validate stock
	for _, req := range request.Items {
		if req.OrderStatusID != 1 {
			err = fmt.Errorf("%s%s", "Paid order found: ", req.ID)
			return
		}

		var prod product.Product
		val, exist := prodStock[req.ProductID]
		if !exist {
			prod, err = ou.ProductRepository.GetProductById(ctx.Context(), req.ProductID)
			if err != nil {
				err = helper.ValidateStruct(err, "product: "+req.ProductID)
				return
			}
		} else {
			prod = val
		}

		fmt.Println("prods: ", prod)
		prod.Stock -= req.Quantity
		user.Cash -= req.Price

		if prod.Stock < 0 {
			err = fmt.Errorf("%s", "Insufficient stock "+req.ProductID)
			return
		}

		prodStock[req.ProductID] = prod
		orderIds = append(orderIds, fmt.Sprintf("'%s'", req.ID))
	}

	// prepare multiple stock sql
	for _, prod := range prodStock {
		prodsql = append(prodsql, fmt.Sprintf("('%s',%d)", prod.ID, prod.Stock))
	}

	// validate cash
	if user.Cash < 0 {
		err = fmt.Errorf("%s", "Not enough balance")
		return
	}

	// update cash
	// update stock
	// update cart status
	tx, err := ou.OrderRepository.BeginTx(ctx.Context())
	if err != nil {
		return
	}

	err = ou.UserRepository.UpdateUserCash(ctx.Context(), tx, user.ID, user.Cash)
	if err != nil {
		tx.Rollback(ctx.Context())
		return
	}

	err = ou.ProductRepository.BulkUpdateProductStock(ctx.Context(), tx, prodsql)
	if err != nil {
		tx.Rollback(ctx.Context())
		return
	}

	err = ou.OrderRepository.UpdateCartStatus(ctx.Context(), tx, orderIds, 2)
	if err != nil {
		tx.Rollback(ctx.Context())
		return
	}

	tx.Commit(ctx.Context())
	return

}

func (ou *OrderUsecase) FindCartByUserId(ctx *fiber.Ctx) (res []order.Order, err error) {

	cookie := ctx.Cookies("token")
	_, userId, _, err := helper.ParseJwt(cookie)
	if err != nil {
		return
	}

	if len(userId) <= 0 {
		err = fmt.Errorf("%s", "Empty Creds")
		return
	}

	_, err = ou.UserRepository.FindUserByParam(ctx.Context(), "id", strings.ToLower(userId))
	if err != nil {
		err = helper.ValidateStruct(err, "user")
		return
	}

	res, err = ou.OrderRepository.FindCartByUserId(ctx.Context(), userId)
	return
}

func (ou *OrderUsecase) DeleteCarts(ctx *fiber.Ctx, request []order.Order) (err error) {

	if len(request) < 0 {
		err = fmt.Errorf("%s", "Empty Charts")
		return
	}

	var productIds []string
	for _, req := range request {
		if req.OrderStatusID != 1 {
			err = fmt.Errorf("%s%s", "Paid order cannot be removed: ", req.ID)
			return
		}
		productIds = append(productIds, fmt.Sprintf("'%s'", req.ID))
	}

	err = ou.OrderRepository.DeleteCarts(ctx.Context(), productIds)
	return
}
