package controller

import (
	"online-store-app-synapsis-2/config"
	"online-store-app-synapsis-2/exception"
	"online-store-app-synapsis-2/middleware"
	"online-store-app-synapsis-2/model"
	"online-store-app-synapsis-2/model/order"
	"online-store-app-synapsis-2/services/orders/usecase"

	"github.com/gofiber/fiber/v2"
)

type OrderController struct {
	OrderUsecase *usecase.OrderUsecase
}

func (oc *OrderController) NewOrderController(app *fiber.App) {

	order := app.Group(config.EndpointPrefixOrder)

	order.Get("/ping", func(ctx *fiber.Ctx) error {
		return ctx.Status(fiber.StatusOK).JSON(model.WebResponse{
			Code:    fiber.StatusOK,
			Status:  true,
			Message: "ok",
		})
	})

	order.Use(middleware.IsAuthenticated)

	order.Get("/cart", oc.FindCartByUserId)
	order.Post("/cart", oc.StoreCartObj)
	order.Put("/cart", oc.CheckoutCartObj)
	order.Delete("/cart", oc.DeleteCarts)
}

func (oc *OrderController) StoreCartObj(ctx *fiber.Ctx) (err error) {
	var request order.OrderCreateDTO

	err = ctx.BodyParser(&request)
	if err != nil {
		return exception.ErrorHandler(ctx, err)
	}

	err = oc.OrderUsecase.StoreCartObj(ctx, request)
	if err != nil {
		return exception.ErrorHandler(ctx, err)
	}

	return ctx.Status(fiber.StatusOK).JSON(model.WebResponse{
		Code:    fiber.StatusCreated,
		Status:  true,
		Message: "Add to cart success",
		Data:    nil,
	})

}

func (oc *OrderController) CheckoutCartObj(ctx *fiber.Ctx) (err error) {
	var request order.OrderCheckoutDTO

	err = ctx.BodyParser(&request)
	if err != nil {
		return exception.ErrorHandler(ctx, err)
	}

	err = oc.OrderUsecase.CheckOutCartObj(ctx, request)
	if err != nil {
		return exception.ErrorHandler(ctx, err)
	}

	return ctx.Status(fiber.StatusOK).JSON(model.WebResponse{
		Code:    fiber.StatusCreated,
		Status:  true,
		Message: "Checkout cart success",
		Data:    nil,
	})

}

func (oc *OrderController) FindCartByUserId(ctx *fiber.Ctx) (err error) {

	res, err := oc.OrderUsecase.FindCartByUserId(ctx)
	if err != nil {
		return exception.ErrorHandler(ctx, err)
	}

	return ctx.Status(fiber.StatusOK).JSON(model.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "Find Cart By User Id success",
		Data:    res,
	})

}

func (oc *OrderController) DeleteCarts(ctx *fiber.Ctx) (err error) {

	var request order.OrderRemoveDTO

	err = ctx.BodyParser(&request)
	if err != nil {
		return exception.ErrorHandler(ctx, err)
	}

	err = oc.OrderUsecase.DeleteCarts(ctx, request.Items)
	if err != nil {
		return exception.ErrorHandler(ctx, err)
	}

	return ctx.Status(fiber.StatusOK).JSON(model.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "Delete Carts success",
		Data:    nil,
	})

}
