package repository

import (
	"context"
	"fmt"
	"online-store-app-synapsis-2/model/user"
	"strings"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgxpool"
)

type UserRepository struct {
	Database *pgxpool.Pool
}

func (ur *UserRepository) BeginTx(ctx context.Context) (db pgx.Tx, err error) {
	db, err = ur.Database.Begin(ctx)
	return
}

func (ur *UserRepository) CreateUser(ctx context.Context, request user.User) (res user.User, err error) {

	tx, err := ur.BeginTx(ctx)
	if err != nil {
		return
	}

	id := "USER" + strings.ToUpper(uuid.NewString()[:8])
	request.ID = id

	query := fmt.Sprintf(`
		INSERT INTO users (id, email, password, cash, created_at)
		VALUES ('%s','%s','%s',%2.f,now())
	`, id, request.Email, request.Password, request.Cash)

	_, err = tx.Exec(ctx, query)
	if err != nil {
		return
	}
	res = request

	tx.Commit(ctx)

	return

}

func (ur *UserRepository) UpdateUserCash(ctx context.Context, tx pgx.Tx, userId string, cash float64) (err error) {

	query := fmt.Sprintf(`
		UPDATE users SET cash = %2.f 
		WHERE id = '%s'
	`, cash, userId)

	_, err = tx.Exec(ctx, query)
	if err != nil {
		return
	}

	return

}

func (ur *UserRepository) FindUserByParam(ctx context.Context, param, args string) (res user.User, err error) {

	tx, err := ur.BeginTx(ctx)
	if err != nil {
		return
	}

	query := fmt.Sprintf(`
		select * from users where lower(%s) = '%s'
	`, param, args)

	rows, err := tx.Query(ctx, query)
	if err != nil {
		return
	}

	res, err = pgx.CollectOneRow(rows, pgx.RowToStructByPos[user.User])
	if err != nil {
		fmt.Printf("CollectRows error: %v", err)
		return
	}

	tx.Commit(ctx)

	return

}
