package controller

import (
	"online-store-app-synapsis-2/config"
	"online-store-app-synapsis-2/exception"
	"online-store-app-synapsis-2/model"
	"online-store-app-synapsis-2/model/user"
	"online-store-app-synapsis-2/services/users/usecase"

	"github.com/gofiber/fiber/v2"
)

type UserController struct {
	UserUsecase *usecase.UserUsecase
}

func (uc *UserController) NewAuthController(app *fiber.App) {
	auth := app.Group(config.EndpointPrefixAuth)

	auth.Get("/ping", func(ctx *fiber.Ctx) error {
		return ctx.Status(fiber.StatusOK).JSON(model.WebResponse{
			Code:    fiber.StatusOK,
			Status:  true,
			Message: "ok",
		})
	})

	auth.Post("/register", uc.Register)
	auth.Post("/login", uc.Login)

}

func (uc *UserController) Register(ctx *fiber.Ctx) (err error) {

	var request user.User

	err = ctx.BodyParser(&request)
	if err != nil {
		return exception.ErrorHandler(ctx, err)
	}

	res, err := uc.UserUsecase.Register(ctx, request)
	if err != nil {
		return exception.ErrorHandler(ctx, err)
	}

	return ctx.Status(fiber.StatusOK).JSON(model.WebResponse{
		Code:    fiber.StatusCreated,
		Status:  true,
		Message: "register success",
		Data:    res,
	})

}

func (uc *UserController) Login(ctx *fiber.Ctx) (err error) {

	var request user.LoginDTO

	err = ctx.BodyParser(&request)
	if err != nil {
		return exception.ErrorHandler(ctx, err)
	}

	res, err := uc.UserUsecase.Login(ctx, request)
	if err != nil {
		return exception.ErrorHandler(ctx, err)
	}

	return ctx.Status(fiber.StatusOK).JSON(model.WebResponse{
		Code:    fiber.StatusCreated,
		Status:  true,
		Message: "login success",
		Data:    res,
	})

}
