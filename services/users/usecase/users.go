package usecase

import (
	"fmt"
	"online-store-app-synapsis-2/config"
	"online-store-app-synapsis-2/exception"
	"online-store-app-synapsis-2/helper"
	"online-store-app-synapsis-2/model/user"
	"online-store-app-synapsis-2/services/users/repository"
	"strconv"
	"strings"
	"time"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
)

/*
   keep on main() stackframe
*/

type UserUsecase struct {
	Validate       *validator.Validate
	UserRepository *repository.UserRepository
}

func (uu *UserUsecase) Register(ctx *fiber.Ctx, request user.User) (res user.User, err error) {

	err = uu.Validate.Struct(request)
	if err != nil {
		err = helper.ValidateStruct(err)
		return
	}
	request.SetPassword(request.Password)

	now := time.Now()
	request.CreatedAt = &now

	res, err = uu.UserRepository.CreateUser(ctx.Context(), request)
	return
}

func (uu *UserUsecase) Login(ctx *fiber.Ctx, request user.LoginDTO) (res user.User, err error) {

	err = uu.Validate.Struct(request)
	if err != nil {
		err = helper.ValidateStruct(err)
		return
	}

	res, err = uu.UserRepository.FindUserByParam(ctx.Context(), "email", strings.ToLower(request.Email))
	if err != nil {
		return
	}

	if len(res.ID) <= 0 {
		err = fmt.Errorf("%s", "Wrong email/pasword")
		return
	}

	err = res.ComparePassword(request.Password)
	if err != nil {
		err = fmt.Errorf("%s", "Wrong email/pasword")
		return
	}

	token, err := helper.GenerateJwt(res.Email, res.ID, "")
	if err != nil {
		err = exception.ErrorBadRequest(err.Error())
		return
	}

	session, _ := strconv.Atoi(config.SessionLogin)
	cookie := fiber.Cookie{
		Name:     "token",
		Value:    token,
		Expires:  time.Now().Add(time.Hour * time.Duration(session)),
		HTTPOnly: true,
	}
	ctx.Cookie(&cookie)

	return
}
