package utils

import (
	"context"
	"log"
	"net/http"
	"os"

	vault "github.com/hashicorp/vault/api"
	"github.com/joho/godotenv"
)

var (
	vaultHost   = os.Getenv(".env")
	vaultPort   = os.Getenv(".env")
	vaultAuth   = os.Getenv(".env")
	vaultToken  = os.Getenv(".env")
	vaultEngine = os.Getenv(".env")
	vaultPath   = os.Getenv(".env")
)

func GetEnv(key string) string {
	valueVault := getVaultEnv(key)
	valueOS := getOSEnv(key)
	valueDotEnv := getDotEnv(key)

	if valueVault != "" {
		return valueVault
	} else if valueOS != "" {
		return valueOS
	} else if valueDotEnv != "" {
		return valueDotEnv
	}

	log.Println("Missing environment variable " + key)
	return ""
}

func getOSEnv(key string) string {
	value := os.Getenv(key)
	if value == "" {
		log.Printf("Missing environment variable %s from OS", key)
		return ""
	}

	return value
}

func getDotEnv(key string) string {
	err := godotenv.Load()
	if err != nil {
		log.Printf("Missing environment variable %s from .env", key)
		return ""
	}

	value, ok := os.LookupEnv(key)
	if !ok {
		return ""
	}
	return value
}

func getVaultEnv(key string) string {

	var vaultURL string

	if vaultHost == "" || vaultPort == "" || vaultAuth == "" || vaultToken == "" || vaultPath == "" {
		log.Printf("Missing environment variable %s from Vault", key)
		return ""
	} else {
		vaultURL = "http://" + vaultHost + ":" + vaultPort

		resp, err := http.Get(vaultURL)
		if err != nil {
			return ""
		}

		defer resp.Body.Close()
	}

	config := vault.DefaultConfig()
	config.Address = vaultURL

	client, err := vault.NewClient(config)
	if err != nil {
		return ""
	}
	client.SetToken(vaultToken)

	secret, err := client.KVv2(vaultEngine).Get(context.Background(), vaultPath)
	if err != nil {
		return ""
	}
	value, ok := secret.Data[key].(string)
	if !ok {
		return ""
	}
	return value
}
